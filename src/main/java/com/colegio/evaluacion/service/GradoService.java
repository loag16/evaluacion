package com.colegio.evaluacion.service;

import com.colegio.evaluacion.model.Grado;
import com.colegio.evaluacion.repository.GradoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 8a
 */
@Service
public class GradoService {

    @Autowired
    GradoRepository repository;

    public Grado save(Grado grado) {
        return repository.save(grado);
    }

    public Grado get(Long id) {
        return repository.findById(id).orElse(new Grado());
    }

    public List<Grado> getAll() {
        return repository.findAll();
    }

    public void delete(Grado grado) {
        repository.delete(grado);
    }
}
