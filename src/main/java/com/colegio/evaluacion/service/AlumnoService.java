package com.colegio.evaluacion.service;

import com.colegio.evaluacion.model.Alumno;
import com.colegio.evaluacion.repository.AlumnoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 8a
 */
@Service
public class AlumnoService {
    
    @Autowired
    AlumnoRepository repository;
    
    public Alumno save(Alumno alumno) {
        return repository.save(alumno);
    }
    
    public Alumno get(Long id) {
        return repository.findById(id).orElse(new Alumno());
    }
    
    public List<Alumno> getAll() {
        return repository.findAll();
    }
    
    public void delete(Alumno alumno) {
        repository.delete(alumno);
    }
}
