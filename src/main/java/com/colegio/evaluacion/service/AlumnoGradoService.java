package com.colegio.evaluacion.service;

import com.colegio.evaluacion.model.AlumnoGrado;
import com.colegio.evaluacion.repository.AlumnoGradoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 8a
 */
@Service
public class AlumnoGradoService {
    
    @Autowired
    AlumnoGradoRepository repository;
    
    public AlumnoGrado save(AlumnoGrado alumnoGrado) {
        return repository.save(alumnoGrado);
    }
 
    public AlumnoGrado get(Long id) {
        return repository.findById(id).orElse(new AlumnoGrado());
    }
    
    public List<AlumnoGrado> getAll() {
        return repository.findAll();
    }
    
    public void delete(AlumnoGrado alumnoGrado) {
        repository.delete(alumnoGrado);
    }
}
