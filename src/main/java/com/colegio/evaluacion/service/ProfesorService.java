package com.colegio.evaluacion.service;

import com.colegio.evaluacion.model.Profesor;
import com.colegio.evaluacion.repository.ProfesorRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 8a
 */
@Service
public class ProfesorService {
    
    @Autowired
    ProfesorRepository repository;
    
    public Profesor save(Profesor profesor) {
        return repository.save(profesor);
    }
    
    public Profesor get(Long id) {
        return repository.findById(id).orElse(new Profesor());
    }
    
    public List<Profesor> getAll() {
        return repository.findAll();
    }
    
    public void delete(Profesor profesor) {
        repository.delete(profesor);
    }
}
