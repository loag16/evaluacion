package com.colegio.evaluacion.constant;

/**
 *
 * @author 8a
 */
public enum Genero {

    MASCULINO("Masculino"),FEMENINO("Femenino");
    
    private String description;

    private Genero(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
