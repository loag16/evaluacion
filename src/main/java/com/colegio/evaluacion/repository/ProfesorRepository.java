package com.colegio.evaluacion.repository;

import com.colegio.evaluacion.model.Profesor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author 8a
 */
@Repository
public interface ProfesorRepository extends JpaRepository<Profesor, Long>{
    
}
