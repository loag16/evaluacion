package com.colegio.evaluacion.repository;

import com.colegio.evaluacion.model.AlumnoGrado;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author 8a
 */
public interface AlumnoGradoRepository extends JpaRepository<AlumnoGrado, Long> {
    
}
