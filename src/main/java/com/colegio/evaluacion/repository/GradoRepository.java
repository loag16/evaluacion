package com.colegio.evaluacion.repository;

import com.colegio.evaluacion.model.Grado;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author 8a
 */
public interface GradoRepository extends JpaRepository<Grado, Long>{
    
}
