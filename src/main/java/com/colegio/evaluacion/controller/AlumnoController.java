package com.colegio.evaluacion.controller;

import com.colegio.evaluacion.constant.Genero;
import com.colegio.evaluacion.model.Alumno;
import com.colegio.evaluacion.service.AlumnoService;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author 8a
 */
@Controller
public class AlumnoController {

    String[] headers = {"ID", "Nombre", "Apellidos", "Género", "Fecha de Nacimiento", ""};
    @Autowired
    AlumnoService service;

    @GetMapping("/alumnos")
    public String getReport(Model model) {

        model.addAttribute("list", service.getAll());
        model.addAttribute("headers", headers);
        model.addAttribute("reportName", "Reporte de alumnos");

        return "alumnos";
    }

    @GetMapping("/alumnos/modal/{id}")
    public String openEdit(Model model, @PathVariable("id") Long id) {

        model.addAttribute("formName", "Edición de alumnos");
        model.addAttribute("alumno", service.get(id));
        model.addAttribute("generos" , Genero.values());
        return "alumnos-modal";
    }

    @GetMapping("/alumnos/modal")
    public String openAdd(Model model) {

        model.addAttribute("formName", "Alta de alumnos");
        model.addAttribute("alumno", new Alumno());
        model.addAttribute("generos" , Genero.values());
        return "alumnos-modal";
    }

    @PostMapping("/alumnos/save")
    public String save(Alumno bean) {
        service.save(bean);

        return "redirect:/alumnos";
    }

    @GetMapping("/alumnos/delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        service.delete(service.get(id));
        return "redirect:/alumnos";
    }

    @InitBinder
    public void init(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }
}
