package com.colegio.evaluacion.controller;

import com.colegio.evaluacion.constant.Genero;
import com.colegio.evaluacion.model.Profesor;
import com.colegio.evaluacion.service.ProfesorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author 8a
 */
@Controller
public class ProfesorController {
    
    String[] headers = {"ID", "Nombre", "Apellidos", "Género", ""};
    @Autowired
    ProfesorService service;

    @GetMapping("/profesores")
    public String getReport(Model model) {

        model.addAttribute("list", service.getAll());
        model.addAttribute("headers", headers);
        model.addAttribute("reportName", "Reporte de profesores");

        return "profesores";
    }

    @GetMapping("/profesores/modal/{id}")
    public String openEdit(Model model, @PathVariable("id") Long id) {

        model.addAttribute("formName", "Edición de profesores");
        model.addAttribute("profesor", service.get(id));
        model.addAttribute("generos" , Genero.values());
        return "profesores-modal";
    }

    @GetMapping("/profesores/modal")
    public String openAdd(Model model) {

        model.addAttribute("formName", "Alta de profesores");
        model.addAttribute("profesor", new Profesor());
        model.addAttribute("generos" , Genero.values());
        return "profesores-modal";
    }

    @PostMapping("/profesores/save")
    public String save(Profesor bean) {
        service.save(bean);

        return "redirect:/profesores";
    }

    @GetMapping("/profesores/delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        service.delete(service.get(id));
        return "redirect:/profesores";
    }
}
