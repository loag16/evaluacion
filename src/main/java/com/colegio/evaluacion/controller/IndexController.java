package com.colegio.evaluacion.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author 8a
 */
public class IndexController {

    @GetMapping("/")
    public String getReport(Model model) {
        
        return "index";
    }
}
