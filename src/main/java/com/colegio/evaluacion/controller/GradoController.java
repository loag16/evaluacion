package com.colegio.evaluacion.controller;

import com.colegio.evaluacion.model.Grado;
import com.colegio.evaluacion.service.GradoService;
import com.colegio.evaluacion.service.ProfesorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author 8a
 */
@Controller
public class GradoController {
    
    String[] headers = {"ID", "Nombre", "Profesor", ""};
    @Autowired
    GradoService service;
    @Autowired
    ProfesorService profesorService;

    @GetMapping("/grados")
    public String getReport(Model model) {

        model.addAttribute("list", service.getAll());
        model.addAttribute("headers", headers);
        model.addAttribute("reportName", "Reporte de grados");

        return "grados";
    }

    @GetMapping("/grados/modal/{id}")
    public String openEdit(Model model, @PathVariable("id") Long id) {

        model.addAttribute("formName", "Edición de grados");
        model.addAttribute("grado", service.get(id));
        model.addAttribute("profesores", profesorService.getAll());
        return "grados-modal";
    }

    @GetMapping("/grados/modal")
    public String openAdd(Model model) {

        model.addAttribute("formName", "Alta de grados");
        model.addAttribute("profesores", profesorService.getAll());
        model.addAttribute("grado", new Grado());
        return "grados-modal";
    }

    @PostMapping("/grados/save")
    public String save(Grado bean) {
        service.save(bean);

        return "redirect:/grados";
    }

    @GetMapping("/grados/delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        service.delete(service.get(id));
        return "redirect:/grados";
    }
}
