package com.colegio.evaluacion.controller;

import com.colegio.evaluacion.model.AlumnoGrado;
import com.colegio.evaluacion.service.AlumnoGradoService;
import com.colegio.evaluacion.service.AlumnoService;
import com.colegio.evaluacion.service.GradoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author 8a
 */
@Controller
public class AlumnoGradoController {

    String[] headers = {"ID", "Alumno", "Grado", "Sección", ""};
    @Autowired
    AlumnoGradoService service;
    
    @Autowired
    AlumnoService alumnoService;
    
    @Autowired
    GradoService gradoService;

    @GetMapping("/alumno-grados")
    public String getReport(Model model) {

        model.addAttribute("list", service.getAll());
        model.addAttribute("headers", headers);
        model.addAttribute("reportName", "Reporte de grado de alumnos");

        return "alumno-grados";
    }

    @GetMapping("/alumno-grados/modal/{id}")
    public String openEdit(Model model, @PathVariable("id") Long id) {

        model.addAttribute("formName", "Edición de grado de alumnos");
        model.addAttribute("alumnoGrado", service.get(id));
        model.addAttribute("alumnos", alumnoService.getAll());
        model.addAttribute("grados", gradoService.getAll());
        return "alumno-grados-modal";
    }

    @GetMapping("/alumno-grados/modal")
    public String openAdd(Model model) {

        model.addAttribute("formName", "Alta de grado del alumno");
        model.addAttribute("alumnoGrado", new AlumnoGrado());
        model.addAttribute("alumnos", alumnoService.getAll());
        model.addAttribute("grados", gradoService.getAll());
        return "alumno-grados-modal";
    }

    @PostMapping("/alumno-grados/save")
    public String save(AlumnoGrado bean) {
        service.save(bean);

        return "redirect:/alumno-grados";
    }

    @GetMapping("/alumno-grados/delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        service.delete(service.get(id));
        return "redirect:/alumno-grados";
    }
}
